#OBJS specifies which files to compile as part of the project
OBJS := src/main.c

#CC specifies which compiler we're using
CC := clang

SDL_CFLAGS := $(shell sdl2-config --cflags)
SDL_LDFLAGS := $(shell sdl2-config --libs)

#COMPILER_FLAGS specifies the additional compilation options we're using
# -w suppresses all warnings
COMPILER_FLAGS := -g -std=c99 -Wall -pedantic $(SDL_CFLAGS)

#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS := $(SDL_LDFLAGS)

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME := hello_SDL

#This is the target that compiles our executable
all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o bin/$(OBJ_NAME)